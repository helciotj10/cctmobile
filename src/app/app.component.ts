import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, Events, AlertController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Network } from '@ionic-native/network';


//import { HomePage } from '../pages/home/home';
import { ParceirosPage } from '../pages/parceiros/parceiros';
import { PontosPage } from '../pages/pontos/pontos';
import { TabsPage } from '../pages/tabs/tabs';
import { MapaPage } from '../pages/mapa/mapa';
import { timer } from 'rxjs/observable/timer';

export interface PageInterface {
  title: string;
  pageName: string;
  tabComponent?: any;
  index?: number;
  icon: string;
}


@Component({
  templateUrl: 'app.html'
})
export class MyApp {
 
  @ViewChild(Nav) nav: Nav;

  rootPage: any = TabsPage;

  pages: PageInterface[] = [
    { title: 'Inicio', pageName: 'TabsPage', tabComponent: 'HomePage', index: 0, icon: 'home'},
    { title: 'Mapa', pageName: 'TabsPage', tabComponent: 'MapaPage', index: 1, icon: 'map'},
    { title: 'Utilizador', pageName: 'TabsPage', tabComponent: 'UtilizadorPage', index: 2, icon: 'person'},
  ];

  paginas: Array<{icone: string, title: string, component: any}>;

  showSplash = true;

  constructor(public platform: Platform, public events: Events, private alertCtrl: AlertController, public network: Network, public statusBar: StatusBar, public splashScreen: SplashScreen) {
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.paginas = [
      { icone: 'home', title: 'Inicio', component: TabsPage },
      { icone: 'map', title: 'Mapa', component: MapaPage},
      { icone: 'people', title: 'Parceiros', component: ParceirosPage },
      { icone: 'locate', title: 'Pontos de Interesse', component: PontosPage }
    ];

    

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.listenConnection();
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      timer(3000).subscribe(() => this.showSplash = false);
    });
  }

  private listenConnection(): void {
    this.network.onDisconnect()
      .subscribe(() => {
        this.showAlert();
      });
  }

  private showAlert(): void {
    const alert = this.alertCtrl.create({
      title: 'Sem acesso a internet!',
      subTitle: 'Desculpe, não estamos conseguindo conectar a internet. Por Favor, Verifique a sua conecção e volte a tentar.',
      buttons: [{
          text: 'OK',
          handler: () => {
            alert.dismiss().then(() => { this.platform.exitApp(); });
          }
        }]
    });
    alert.present();
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }
}
