import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';
import { HttpModule } from '@angular/http';
import { Camera } from '@ionic-native/camera';
import { FileTransfer, FileTransferObject } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';
import { Network } from '@ionic-native/network';
import { IonicImageLoader } from 'ionic-image-loader';
import { IonicImageViewerModule } from 'ionic-img-viewer';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import { ParceirosPage } from '../pages/parceiros/parceiros';
import { PontosPage } from '../pages/pontos/pontos';
import { VisitaPage } from '../pages/visita/visita';
import { HistoriaPage } from '../pages/historia/historia';
import { TestemunhosPage } from '../pages/testemunhos/testemunhos';
import { GaleriaPage } from '../pages/galeria/galeria';
import { InformacoesPage } from '../pages/informacoes/informacoes';
import { PopoverComponent } from '../components/popover/popover';
import { TabsPage } from '../pages/tabs/tabs';
import { MapaPage } from '../pages/mapa/mapa';
import { UtilizadorPage } from '../pages/utilizador/utilizador';
import { CriarcontaPage } from '../pages/criarconta/criarconta';


import { TabsuserPage } from '../pages/tabsuser/tabsuser';
import { TodosPage } from '../pages/todos/todos';
import { PublicarPage } from '../pages/publicar/publicar';
import { MinhaspublicacoesPage } from '../pages/minhaspublicacoes/minhaspublicacoes';
import { PerfilPage } from '../pages/perfil/perfil';


import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { ProvedorProvider } from '../providers/provedor/provedor';



@NgModule({
  declarations: [
  MyApp,
  HomePage,
  ListPage,
  ParceirosPage,
  PontosPage,
  VisitaPage,
  HistoriaPage,
  TestemunhosPage,
  GaleriaPage,
  InformacoesPage,
  PopoverComponent,
  TabsPage,
  MapaPage,
  UtilizadorPage,
  TabsuserPage,
  TodosPage,
  PublicarPage,
  MinhaspublicacoesPage,
  PerfilPage,
  CriarcontaPage
  ],
  imports: [
  BrowserModule,
  IonicModule.forRoot(MyApp),
  HttpModule,
  IonicImageViewerModule,
  IonicImageLoader.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
  MyApp,
  HomePage,
  ListPage,
  ParceirosPage,
  PontosPage,
  VisitaPage,
  HistoriaPage,
  TestemunhosPage,
  GaleriaPage,
  InformacoesPage,
  PopoverComponent,
  TabsPage,
  MapaPage,
  UtilizadorPage,
  TabsuserPage,
  TodosPage,
  PublicarPage,
  MinhaspublicacoesPage,
  PerfilPage,
  CriarcontaPage
  ],
  providers: [
  Camera,
  FileTransfer,
  FileTransferObject,
  File,
  Network,
  StatusBar,
  SplashScreen,
  {provide: ErrorHandler, useClass: IonicErrorHandler},
    Geolocation,
    ProvedorProvider,
  ]
})
export class AppModule {}
