import { Http, Response, Headers } from '@angular/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import { Observable } from 'rxjs/Observable';

/*
  Generated class for the ProvedorProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ProvedorProvider {

  private url: string = "https://www.cctvirtual.cf/downloads/";

  constructor(private http: Http) {
    console.log('Hello ProvedorProvider Provider');
  }

  getFhistoria(){
  	return this.http.get(this.url + "fhistoria.php")
  	.map(this.extractData)
  	.do(this.logResponse)
  	.catch(this.catchError);
  }

  getHistoria(){
    return this.http.get(this.url + "historia.php")
    .map(this.extractData)
    .do(this.logResponse)
    .catch(this.catchError);
  }

  getPublicacoes(id, lista){
    return this.http.get(this.url + "publicacoes.php?e="+id+"&l="+lista)
    .map(this.extractData)
    .do(this.logResponse)
    .catch(this.catchError);
  }

  getComentario(id){
    return this.http.get(this.url + "getcomentario.php?e="+id)
    .map(this.extractData)
    .do(this.logResponse)
    .catch(this.catchError);
  }

  delComentario(id, idP){
    return this.http.get(this.url + "delcomentario.php?e="+id+"&p="+idP)
    .map(this.extractData)
    .do(this.logResponse)
    .catch(this.catchError);
  }

  delPublicacao(id){
    return this.http.get(this.url + "delpublicacao.php?e="+id)
    .map(this.extractData)
    .do(this.logResponse)
    .catch(this.catchError);
  }

  setComentario(idU, idP, co){
    return this.http.get(this.url + "comentario.php?e="+idU+"&p="+idP+"&c="+co)
    .map(this.extractData)
    .do(this.logResponse)
    .catch(this.catchError);
  }

   setPassword(idU, idP){
    return this.http.get(this.url + "changepass.php?e="+idU+"&p="+idP)
    .map(this.extractData)
    .do(this.logResponse)
    .catch(this.catchError);
  }

  setlike(idU,idP){
    return this.http.get(this.url + "like.php?e="+idU+"&p="+idP)
    .map(this.extractData);
  }

  getminhasPublicacoes(id, lista){
    return this.http.get(this.url + "minhaspublicacoes.php?e="+id+"&l="+lista)
    .map(this.extractData)
    .do(this.logResponse)
    .catch(this.catchError);
  }

  getTestemunhos(){
    return this.http.get(this.url + "testemunhos.php")
    .map(this.extractData)
    .do(this.logResponse)
    .catch(this.catchError);
  }

  getGaleria(){
    return this.http.get(this.url + "galeria.php")
    .map(this.extractData)
    .do(this.logResponse)
    .catch(this.catchError);
  }

  getInformacoes(){
    return this.http.get(this.url + "informacao.php")
    .map(this.extractData)
    .do(this.logResponse)
    .catch(this.catchError);
  }

  getPontos(){
    return this.http.get(this.url + "pontos.php")
    .map(this.extractData)
    .do(this.logResponse)
    .catch(this.catchError);
  }

  getUtilizador(credentials){
    return this.http.get(this.url + credentials)
    .map(this.extractData);
  }

 setpublicacao(credentials) {
    return new Promise((resolve, reject) => {
      let headers = new Headers();

      this.http.post(this.url+"upload.php", JSON.stringify(credentials), {headers: headers})
        .subscribe(res => {
          resolve(res.json());
        }, (err) => {
          reject(err);
        });
    });
  }

  updatePublicacao(credentials) {
    return new Promise((resolve, reject) => {
      let headers = new Headers();

      this.http.post(this.url+"uploadUpdate.php", JSON.stringify(credentials), {headers: headers})
        .subscribe(res => {
          resolve(res.json());
        }, (err) => {
          reject(err);
        });
    });
  }

  editPublicacao(credentials) {
    return new Promise((resolve, reject) => {
      let headers = new Headers();

      this.http.post(this.url+"editpublicacao.php", JSON.stringify(credentials), {headers: headers})
        .subscribe(res => {
          resolve(res.json());
        }, (err) => {
          reject(err);
        });
    });
  }

  setfotoperfil(credentials) {
    return new Promise((resolve, reject) => {
      let headers = new Headers();

      this.http.post(this.url+"uploadPhoto.php", JSON.stringify(credentials), {headers: headers})
        .subscribe(res => {
          resolve(res.json());
        }, (err) => {
          reject(err);
        });
    });
  }

  setConta(credentials) {
    return new Promise((resolve, reject) => {
      let headers = new Headers();

      this.http.post(this.url+"signup.php", JSON.stringify(credentials), {headers: headers})
        .subscribe(res => {
          resolve(res.json());
        }, (err) => {
          reject(err);
        });
    });

  }
  

  private catchError(error: Response | any){
  	console.log(error);
  	return Observable.throw(error.json().error || "Erro no Servidor.")
  }

  private logResponse(res: Response){
  	 console.log(res);
  }

  private extractData(res: Response){
  	return res.json();
  }

}
