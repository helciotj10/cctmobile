import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ModaltodosPage } from './modaltodos';

@NgModule({
  declarations: [
    ModaltodosPage,
  ],
  imports: [
    IonicPageModule.forChild(ModaltodosPage),
  ],
})
export class ModaltodosPageModule {}
