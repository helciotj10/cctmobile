import { Component } from '@angular/core';
import { IonicPage, ViewController, NavParams, ActionSheetController } from 'ionic-angular';
import { ImageViewerController } from 'ionic-img-viewer';
import { ProvedorProvider } from '../../providers/provedor/provedor';
/**
 * Generated class for the ModaltodosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-modaltodos',
  templateUrl: 'modaltodos.html',
})
export class ModaltodosPage {

	dados: any;
  coments: any;
  comentario: any;
  idU: any;
  _imageViewerCtrl: ImageViewerController;

  constructor(private view: ViewController, public actionSheetCtrl: ActionSheetController, private provedor: ProvedorProvider, public navParams: NavParams, imageViewerCtrl: ImageViewerController) {
    var userdata = localStorage.getItem('userData');
    let user = JSON.parse(userdata);
    this.idU = user.id;
    this._imageViewerCtrl = imageViewerCtrl;
  }

  ionViewWillLoad() {
     this.dados = this.navParams.get('data'); 
     this.getComentario(this.dados.id);
  }

  closeModal(){
  	this.view.dismiss();
  }

  setlike(idU, idP){
    this.provedor.setlike(idU, idP).subscribe();
  }

  getComentario(idP){
    this.provedor.getComentario(idP).subscribe(data => this.coments = data);
  }

  delComentario(id, idP){
    this.provedor.delComentario(id, idP).subscribe();
  }

  setComentario(){
    this.provedor.setComentario(this.idU, this.dados.id, this.comentario).subscribe();
    this.getComentario(this.dados.id);
    this.dados.comentario = parseInt(this.dados.comentario) + 1;
    this.comentario = "";
  }

  tapEvent(event, id, like) {

    if(like == 'true'){
          this.dados.like = 'false';
          this.dados.gosto = this.dados.gosto - 1;
          this.setlike(this.idU, id);
    }else{
          this.dados.like = 'true';
          this.dados.gosto = this.dados.gosto + 1;
          this.setlike(this.idU, id);
      }
  } 

  pressEvent(e, id, idC) {

    if( this.idU == id){

      const actionSheet = this.actionSheetCtrl.create({
      title: 'Queres apagar este comentario?',
        buttons: [
          {
            text: 'Apagar',
            role: 'destructive',
            handler: () => {
              this.delComentario(idC, this.dados.id);
              this.getComentario(this.dados.id);
              this.dados.comentario = parseInt(this.dados.comentario) - 1;
            }
          },{
            text: 'Cancelar',
            role: 'cancel',
            handler: () => {
              console.log('Cancel clicked');
            }
          }
        ]
      });
      actionSheet.present();
    }
  }


  presentImage(myImage) {
    const imageViewer = this._imageViewerCtrl.create(myImage);
    imageViewer.present();
  }

}
