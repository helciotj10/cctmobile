import { Component } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';
import { UtilizadorPage } from '../utilizador/utilizador';
import { ProvedorProvider } from '../../providers/provedor/provedor';


/**
 * Generated class for the CriarcontaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


 @Component({
  selector: 'page-criarconta',
  templateUrl: 'criarconta.html',
})
 export class CriarcontaPage {
  responseData : any;
  data = {"username": "", "first": "", "last": "", "email": "", "password": ""};

  constructor(public navCtrl: NavController, public navParams: NavParams,  private provedor: ProvedorProvider, public alertCtrl: AlertController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CriarcontaPage');
  }


  signup(){
   this.provedor.setConta(this.data).then((result) => {
    this.responseData = result;
    if(this.responseData.result.created=="1"){
      //localStorage.setItem('userData', JSON.stringify(this.responseData));
      this.navCtrl.push(UtilizadorPage);
    }
    else{ 

      const alert = this.alertCtrl.create({
        title: 'Este utilizador já existe!',
        subTitle: 'Por favor, tente uma conta diferente ou faça o login.',
        buttons: ['OK']
      });
      alert.present();
    }
}, (err) => {
  console.log("Erro");
});
 }


}
