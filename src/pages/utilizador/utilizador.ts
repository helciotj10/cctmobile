import { Component, ViewChild } from '@angular/core';
import { Nav, NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';
import { TabsuserPage } from '../tabsuser/tabsuser';
import { CriarcontaPage } from '../criarconta/criarconta';
import { ProvedorProvider } from '../../providers/provedor/provedor';



@Component({
  selector: 'page-utilizador',
  templateUrl: 'utilizador.html',
})
export class UtilizadorPage {
	
	@ViewChild(Nav) nav: Nav;
	rootPage: any = TabsuserPage;
  utilizador: any;
  data = {"email": "", "password":""};
  


  constructor(public navCtrl: NavController, public navParams: NavParams, private loadingCtrl:LoadingController, private provedor: ProvedorProvider, public alertCtrl: AlertController) {
  }

  login(){
    let loader = this.loadingCtrl.create({
        content: "A carregar..."
      });
      loader.present();

    if(!this.data.email || !this.data.password){
       const alert = this.alertCtrl.create({
          title: 'Falha no Login!',
          subTitle: 'Por favor, Preenche os campos de email e palavra passe.',
          buttons: ['OK']
        });
        loader.dismiss();
        alert.present();
    }else{
     this.provedor.getUtilizador('login.php?e='+this.data.email+'&p='+this.data.password).subscribe((result) => {
      this.utilizador=result;
      if(this.utilizador.records=='0'){
        const alert = this.alertCtrl.create({
          title: 'Falha no Login!',
          subTitle: 'O email ou a palavra passe estão errados.',
          buttons: ['OK']
        });
        loader.dismiss();
        alert.present();
      }else{
       localStorage.setItem('userData', JSON.stringify(this.utilizador.records));
        loader.dismiss();
       this.navCtrl.push(TabsuserPage);
     }
   }); 
   }
 }
 goCriar(){
   this.navCtrl.push(CriarcontaPage);
 }


 ionViewDidLoad() {
  console.log('ionViewDidLoad UtilizadorPage');
}

}
