import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ModalpontosPage } from './modalpontos';

@NgModule({
  declarations: [
    ModalpontosPage,
  ],
  imports: [
    IonicPageModule.forChild(ModalpontosPage),
  ],
})
export class ModalpontosPageModule {}
