import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavParams, ViewController } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';

declare var google;

/**
 * Generated class for the ModalpontosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-modalpontos',
  templateUrl: 'modalpontos.html',
})
export class ModalpontosPage {

  @ViewChild('map') mapElement: ElementRef;
  map: any;
	dados: any;
  directionsService = new google.maps.DirectionsService;
  directionsDisplay = new google.maps.DirectionsRenderer;

  origin: any;
  destination: any;

  constructor(public navParams: NavParams, private view: ViewController, private geolocation: Geolocation) {
  }

  ionViewWillLoad() {
     this.dados = this.navParams.get('data');
     this.initMap(); 
  }

  closeModal(){
  	this.view.dismiss();
  }

  initMap() {

    this.geolocation.getCurrentPosition({enableHighAccuracy: true})
    .then((resp) => {
      const position = new google.maps.LatLng(resp.coords.latitude, resp.coords.longitude);
      const positionFinal = new google.maps.LatLng(this.dados.latude, this.dados.longitude);

      this.origin = position;
      this.destination = positionFinal;

      this.map = new google.maps.Map(this.mapElement.nativeElement, {
        zoom: 10, 
        mapTypeId: google.maps.MapTypeId.SATELLITE,
        center: {lat: resp.coords.latitude, lng: resp.coords.longitude}
      });

      var marker = new google.maps.Marker({
        position: position,
        map: this.map,
        animation: google.maps.Animation.BOUNCE,
      });

      marker = new google.maps.Marker({
        position: positionFinal,
        map: this.map,
      });
      
      marker.setMap(this.map);

      this.directionsDisplay.setOptions({suppressMarkers: true });
      this.directionsDisplay.setMap(this.map);

      this.directionsService.route({
        origin: position,
        destination: positionFinal,
        travelMode: 'DRIVING'
      }, (response, status) => {
        if (status === 'OK') {
          this.directionsDisplay.setDirections(response);
        } else {
          window.alert('Directions request failed due to ' + status);
        }
      });


    }).catch((error) => {
      console.log('Erro ao recuperar sua posição', error);
    });


       //this.calculateAndDisplayRoute();

  }


  calculateAndDisplayRoute() {
    this.directionsService.route({
      origin: this.origin,
      destination: this.destination,
      travelMode: 'DRIVING'
    }, (response, status) => {
      if (status === 'OK') {
        this.directionsDisplay.setDirections(response);
      } else {
        window.alert('Directions request failed due to ' + status);
      }
    });
  }

}
