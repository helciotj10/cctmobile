import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { ProvedorProvider } from '../../providers/provedor/provedor';

/**
 * Generated class for the InformacoesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-informacoes',
  templateUrl: 'informacoes.html',
})
export class InformacoesPage {
	info = [];

  constructor(public navCtrl: NavController, public navParams: NavParams,  private provedor: ProvedorProvider) {
  	this.getInformacoes(); 
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad InformacoesPage');
  }

  getInformacoes(){
  	this.provedor.getInformacoes().subscribe(data => this.info = data);
  }

}
