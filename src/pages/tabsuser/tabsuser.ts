import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { HomePage } from '../home/home';
import { TodosPage } from '../todos/todos';
import { PublicarPage } from '../publicar/publicar';
import { MinhaspublicacoesPage } from '../minhaspublicacoes/minhaspublicacoes';
import { PerfilPage } from '../perfil/perfil';


@Component({
  selector: 'page-tabsuser',
  templateUrl: 'tabsuser.html',
})
export class TabsuserPage {
	tab1Root = HomePage;
	tab2Root = TodosPage;
	tab3Root = PublicarPage;
	tab4Root = MinhaspublicacoesPage;
	tab5Root = PerfilPage;
	myIndex: number = 1;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

}
