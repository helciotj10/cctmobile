import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, AlertController, ActionSheetController } from 'ionic-angular';
import { Camera, CameraOptions } from '@ionic-native/camera';

import { ProvedorProvider } from '../../providers/provedor/provedor';
//import { File } from '@ionic-native/file';

/**
 * Generated class for the PublicarPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


 @Component({
 	selector: 'page-publicar',
 	templateUrl: 'publicar.html',
 })
 export class PublicarPage {

 	myphoto:any;
  responseData: any;
  data: any = [];

  constructor(public navCtrl: NavController, public actionSheetCtrl: ActionSheetController, public navParams: NavParams, private provedor: ProvedorProvider, private camera: Camera,  private loadingCtrl:LoadingController, public alertCtrl: AlertController) {
  }

  presentActionSheet() {
    const actionSheet = this.actionSheetCtrl.create({
      title: 'Carregar Imagem',
      buttons: [
        {
          text: 'Tirar Uma Foto',
          handler: () => {
            this.takePhoto();
          }
        },{
          text: 'Adicionar da galeria',
          handler: () => {
            this.getImage();
          }
        },{
          text: 'Cortar Imagem',
          handler: () => {
            this.cropImage();
          }
        },{
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            //console.log('Cancel clicked');
          }
        }
      ]
    });
    actionSheet.present();
  }


  takePhoto(){
    const options: CameraOptions = {
      quality: 70,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }

    this.camera.getPicture(options).then((imageData) => {
	      // imageData is either a base64 encoded string or a file URI
	      // If it's base64:
	      this.myphoto = 'data:image/jpeg;base64,' + imageData;
      }, (err) => {
        const alert = this.alertCtrl.create({
          title: 'Erro',
          subTitle: 'Desculpa, ocorreu um erro ao realizar seu pedido.',
          buttons: ['OK']
        });
        alert.present();
      });
  }

  getImage() {
      const options: CameraOptions = {
        quality: 70,
        destinationType: this.camera.DestinationType.DATA_URL,
        sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
        saveToPhotoAlbum:false
      }

      this.camera.getPicture(options).then((imageData) => {
      		      // imageData is either a base64 encoded string or a file URI
      		      // If it's base64:
      		      this.myphoto = 'data:image/jpeg;base64,' + imageData;

              }, (err) => {
               const alert = this.alertCtrl.create({
                title: 'Erro',
                subTitle: 'Desculpa, ocorreu um erro ao realizar seu pedido.',
                buttons: ['OK']
              });
               alert.present();
      });
  }

  cropImage() {
      const options: CameraOptions = {
        quality: 70,
        destinationType: this.camera.DestinationType.DATA_URL,
        sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
        saveToPhotoAlbum: false,
        allowEdit:true,
        targetWidth:300,
        targetHeight:300
      }

      this.camera.getPicture(options).then((imageData) => {
      	      // imageData is either a base64 encoded string or a file URI
      	      // If it's base64:
      	      this.myphoto = 'data:image/jpeg;base64,' + imageData;
           }, (err) => {
             const alert = this.alertCtrl.create({
              title: 'Erro',
              subTitle: 'Desculpa, ocorreu um erro ao realizar seu pedido.',
              buttons: ['OK']
            });
             alert.present();
           });
  }

  uploadImage(){

      if(!this.data.texto && !this.myphoto){
        const alert = this.alertCtrl.create({
            title: 'Erro!',
            subTitle: 'Por Favor, preencha os campos.',
            buttons: ['OK']
          });
          alert.present();
      }else{
         //Show loading
      let loader = this.loadingCtrl.create({
        content: "A carregar..."
      });
      loader.present();

      var userdata = localStorage.getItem('userData');
      let user = JSON.parse(userdata);
     // console.log(user.id);


      let postData = {
        "texto": this.data.texto,
        "foto": this.myphoto,
        "idUser": user.id

      }

      


      this.provedor.setpublicacao(postData).then((result) => {
        this.responseData = result;
        if(this.responseData.result.created=="1"){
          const alert = this.alertCtrl.create({
            title: 'Sucesso!',
            subTitle: 'A publicação foi realizada com sucesso.',
            buttons: ['OK']
          });
          loader.dismiss();
          alert.present();
        //this.navCtrl.push(TabsuserPage);
      }
      else{ 

        const alert = this.alertCtrl.create({
          title: 'Falha!',
          subTitle: 'Desculpa, ocorreu um erro ao realizar seu pedido.',
          buttons: ['OK']
        });
        loader.dismiss();
        alert.present();
      }
      }, (err) => {
        const alert = this.alertCtrl.create({
          title: 'Erro!',
          subTitle: 'Desculpa, ocorreu um erro ao realizar seu pedido.',
          buttons: ['OK']
        });
        loader.dismiss();
        alert.present();
      });
      }
     
  }


}
