import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { VisitaPage } from '../visita/visita';
import { HistoriaPage } from '../historia/historia';
import { TestemunhosPage } from '../testemunhos/testemunhos';
import { GaleriaPage } from '../galeria/galeria';
import { InformacoesPage } from '../informacoes/informacoes';
import { ProvedorProvider } from '../../providers/provedor/provedor';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  foto = [];

  pages: Array<{icone: string, title: string, component: any}>;

  constructor(public navCtrl: NavController, private provedor: ProvedorProvider) {
  		 // used for an example of ngFor and navigation
    this.pages = [
      { icone: 'navigate', title: 'Visita Guiada', component: VisitaPage },
      { icone: 'bookmark', title: 'Historia', component: HistoriaPage },
      { icone: 'microphone', title: 'Depoimentos', component: TestemunhosPage },
      { icone: 'images', title: 'Galeria', component: GaleriaPage },
  	  { icone: 'information-circle', title: 'Informações', component: InformacoesPage },
    ];

    this.getHistoria();
  }


   getHistoria(){
      this.provedor.getFhistoria().subscribe(data => this.foto = data);
    }


  

  goTo(page){
  	this.navCtrl.push(page.component);
  }
}
