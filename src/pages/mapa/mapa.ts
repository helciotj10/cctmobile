import { Component, ViewChild, ElementRef } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';

declare var google;


@Component({
  selector: 'page-mapa',
  templateUrl: 'mapa.html',
})
export class MapaPage {

  @ViewChild('map') mapElement: ElementRef;
  map: any;
  start = 'Ex-Colónia Penal do Tarrafal, Chão Bom';
  end = 'Ex-Colónia Penal do Tarrafal, Chão Bom';
  directionsService = new google.maps.DirectionsService;
  directionsDisplay = new google.maps.DirectionsRenderer;


  constructor(public navCtrl: NavController, public navParams: NavParams, private geolocation: Geolocation) {
  }

  ionViewDidLoad() {
     this.initMap();
     //this.calculateAndDisplayRoute();
  }

  initMap() {

    //this.geolocation.getCurrentPosition({enableHighAccuracy: true})
    this.geolocation.getCurrentPosition({enableHighAccuracy: true})
    .then((resp) => {
      //this.start = resp.coords.latitude + "," + resp.coords.longitude;
      const position = new google.maps.LatLng(resp.coords.latitude, resp.coords.longitude);
      const positionFinal = new google.maps.LatLng(15.264090, -23.743697);

      this.map = new google.maps.Map(this.mapElement.nativeElement, {
        zoom: 10, 
        mapTypeId: google.maps.MapTypeId.SATELLITE,
        center: {lat: resp.coords.latitude, lng: resp.coords.longitude}
      });

      var marker = new google.maps.Marker({
        position: position,
        map: this.map,
        animation: google.maps.Animation.BOUNCE,
      });

      marker = new google.maps.Marker({
        position: positionFinal,
        map: this.map,
      });
      
      marker.setMap(this.map);

      this.directionsDisplay.setOptions({suppressMarkers: true });
      this.directionsDisplay.setMap(this.map);

      this.directionsService.route({
        origin: position,
        destination: positionFinal,
        travelMode: 'DRIVING'
      }, (response, status) => {
        if (status === 'OK') {
          this.directionsDisplay.setDirections(response);
        } else {
          window.alert('Directions request failed due to ' + status);
        }
      });


    }).catch((error) => {
      console.log('Erro ao recuperar sua posição', error);
    });

      /*

                let watch = this.geolocation.watchPosition();
        watch.subscribe((data) => {
         // data can be a set of coordinates, or an error (if an error occurred).
         // data.coords.latitude
         // data.coords.longitude
        });

        */
  }


  calculateAndDisplayRoute() {
    this.directionsService.route({
      origin: this.start,
      destination: this.end,
      travelMode: 'DRIVING'
    }, (response, status) => {
      if (status === 'OK') {
        this.directionsDisplay.setDirections(response);
      } else {
        window.alert('Directions request failed due to ' + status);
      }
    });
  }
}
