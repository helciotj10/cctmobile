import { Component } from '@angular/core';
import { NavController, NavParams, ActionSheetController, ModalController } from 'ionic-angular';
import { ProvedorProvider } from '../../providers/provedor/provedor';
import { ImageViewerController } from 'ionic-img-viewer';

/**
 * Generated class for the MinhaspublicacoesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@Component({
  selector: 'page-minhaspublicacoes',
  templateUrl: 'minhaspublicacoes.html',
})
export class MinhaspublicacoesPage {

	publicacoes = [];
  public tap: number = 0;
  idU: any;
  lista: number = 1;
  _imageViewerCtrl: ImageViewerController;

  constructor(public navCtrl: NavController, public navParams: NavParams, private provedor: ProvedorProvider, private modal: ModalController, private actionSheetCtrl: ActionSheetController, imageViewerCtrl: ImageViewerController) {
  	var userdata = localStorage.getItem('userData');
    let user = JSON.parse(userdata);
    this.idU = user.id;
    this.getminhasPublicacoes(user.id, this.lista);
    this._imageViewerCtrl = imageViewerCtrl;

  }

  getminhasPublicacoes(id, lista){
    this.provedor.getminhasPublicacoes(id, lista).subscribe(data => this.publicacoes = data);
  }

  delPublicacao(id){
    this.provedor.delPublicacao(id).subscribe();
  }

  setlike(idU, idP){
    this.provedor.setlike(idU, idP).subscribe();
  }

  tapEvent(event, id, like) {

    if(like == 'true'){
      for(let p of this.publicacoes) {
        if(p.id == id) {
          p.like = 'false';
          p.gosto = p.gosto - 1;
          console.log(p.like);
          this.setlike(this.idU, id);
        }
      }
    }else{
      for(let p of this.publicacoes) {
        if(p.id == id) {
          p.like = 'true';
          p.gosto = p.gosto + 1;
          console.log(p.like);
          this.setlike(this.idU, id);
        }
      }
    } 
  }

  openModal(t){
    const myModal = this.modal.create('ModaltodosPage', {data: t});
    myModal.present();
  }

  openModalEdit(t){
    const myModal = this.modal.create('ModalEditPubPage', {data: t});
    myModal.present();
  }

  presentImage(myImage) {
    const imageViewer = this._imageViewerCtrl.create(myImage);
    imageViewer.present();
  }

  openMenu(p, id) {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Menu',
      cssClass: 'action-sheets-basic-page',
      buttons: [
        {
          text: 'Apagar Publicação',
          role: 'destructive',
          icon: 'trash',
          handler: () => {
            this.delPublicacao(id);
            this.lista = 1;
            this.getminhasPublicacoes(this.idU, this.lista);
          }
        },
        {
          text: 'Editar',
          icon: 'create',
          handler: () => {
            this.openModalEdit(p);
          }
        },
        {
          text: 'Cancel',
          role: 'cancel', // will always sort to be on the bottom
          icon: 'close',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    actionSheet.present();
  }

  doRefresh(refresher) {
    this.lista = 1;
    this.provedor.getminhasPublicacoes(this.idU, this.lista).subscribe(data => { this.publicacoes = data; refresher.complete();});
  }

  doInfinite(infiniteScroll) {
    this.lista = this.lista + 10;
    this.provedor.getminhasPublicacoes(this.idU, this.lista).subscribe(data => { 
      if(data.result == "0"){infiniteScroll.complete();}
      else{this.publicacoes = this.publicacoes.concat(data); infiniteScroll.complete();}
    });

  }

}
