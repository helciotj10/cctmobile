import { Component } from '@angular/core';
import { NavParams, ModalController } from 'ionic-angular';
import { ProvedorProvider } from '../../providers/provedor/provedor';


/**
 * Generated class for the TestemunhosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


 @Component({
 	selector: 'page-testemunhos',
 	templateUrl: 'testemunhos.html',
 })
 export class TestemunhosPage {
 	testemunho = [];
 	cv = "Cabo-verdiana";
 	ang = "Angolana";
 	gui = "Guinieense";
 	constructor(public navParams: NavParams, private provedor: ProvedorProvider, private modal: ModalController) {
 		this.getTestemunho();
 	}

 	getTestemunho(){
 		this.provedor.getTestemunhos().subscribe(data => this.testemunho = data);
 	} 

 	openModal(t){
 		const myModal = this.modal.create('ModalPage', {data: t});
 		myModal.present();
 	}

 	onInput(ev) {
 		// Reset items back to all of the items
 		//this.getTestemunho();
 		//this.reset();
 		// set val to the value of the searchbar
 		const val = ev.target.value;

 		// if the value is an empty string don't filter the items
 		if (val && val.trim() != '') {
 			this.testemunho = this.testemunho.filter((item) => {
 				console.log("item----"+item.nome.toLowerCase());
 				return (item.nome.toLowerCase().indexOf(val.toLowerCase()) > -1);
 			})
 		}else{
 			this.getTestemunho();
 		}
 	}
 	
 }

