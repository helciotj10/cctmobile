import { Component } from '@angular/core';
import { NavParams, ModalController } from 'ionic-angular';
import { ProvedorProvider } from '../../providers/provedor/provedor';


/**
 * Generated class for the PontosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


 @Component({
  selector: 'page-pontos',
  templateUrl: 'pontos.html',
})
 export class PontosPage {
   pontos = [];

   constructor(public navParams: NavParams, private provedor: ProvedorProvider, private modal: ModalController) {
     this.getPontos();
   }

   getPontos(){
    this.provedor.getPontos().subscribe(data => this.pontos = data);
  }

  openModal(p){
    const myModal = this.modal.create('ModalpontosPage', {data: p});
    myModal.present();
  }
}
