import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, AlertController, ActionSheetController } from 'ionic-angular';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { ProvedorProvider } from '../../providers/provedor/provedor';
import { SplashScreen } from '@ionic-native/splash-screen';

//import { UtilizadorPage } from '../utilizador/utilizador';


@Component({
  selector: 'page-perfil',
  templateUrl: 'perfil.html',
})
export class PerfilPage {

  myphoto:any;
  responseData: any;
  user: any = [];
  qual: boolean = true;

  constructor(public navCtrl: NavController, public actionSheetCtrl: ActionSheetController, public splashScreen: SplashScreen, public navParams: NavParams, private provedor: ProvedorProvider, private camera: Camera,  private loadingCtrl:LoadingController, public alertCtrl: AlertController) {
    var userdata = localStorage.getItem('userData');
    let userd = JSON.parse(userdata);
    this.user = userd;
  }

  presentActionSheet() {
    const actionSheet = this.actionSheetCtrl.create({
      title: 'Carregar Imagem',
      buttons: [
      {
        text: 'Tirar Uma Foto',
        handler: () => {
          this.takePhoto();
        }
      },{
        text: 'Adicionar da galeria',
        handler: () => {
          this.getImage();
        }
      },{
        text: 'Cortar Imagem',
        handler: () => {
          this.cropImage();
        }
      },{
        text: 'Cancel',
        role: 'cancel',
        handler: () => {
          //console.log('Cancel clicked');
        }
      }
      ]
    });
    actionSheet.present();
  }

  takePhoto(){
    const options: CameraOptions = {
      quality: 70,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }

    this.camera.getPicture(options).then((imageData) => {
      // imageData is either a base64 encoded string or a file URI
      // If it's base64:
      this.myphoto = 'data:image/jpeg;base64,' + imageData;
      this.uploadImage();
    }, (err) => {
      const alert = this.alertCtrl.create({
        title: 'Erro',
        subTitle: 'Desculpa, ocorreu um erro ao realizar seu pedido.',
        buttons: ['OK']
      });
      alert.present();
    });
  }

  getImage() {
    const options: CameraOptions = {
      quality: 70,
      destinationType: this.camera.DestinationType.DATA_URL,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      saveToPhotoAlbum:false
    }

    this.camera.getPicture(options).then((imageData) => {
      // imageData is either a base64 encoded string or a file URI
      // If it's base64:
      this.myphoto = 'data:image/jpeg;base64,' + imageData;
      this.uploadImage();

    }, (err) => {
     const alert = this.alertCtrl.create({
      title: 'Erro',
      subTitle: 'Desculpa, ocorreu um erro ao realizar seu pedido.',
      buttons: ['OK']
    });
     alert.present();
   });
  }

  cropImage() {
    const options: CameraOptions = {
      quality: 70,
      destinationType: this.camera.DestinationType.DATA_URL,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      saveToPhotoAlbum: false,
      allowEdit:true,
      targetWidth:300,
      targetHeight:300
    }

    this.camera.getPicture(options).then((imageData) => {
      // imageData is either a base64 encoded string or a file URI
      // If it's base64:
      this.myphoto = 'data:image/jpeg;base64,' + imageData;
      this.uploadImage(); 
    }, (err) => {
     const alert = this.alertCtrl.create({
      title: 'Erro',
      subTitle: 'Desculpa, ocorreu um erro ao realizar seu pedido.',
      buttons: ['OK']
    });
     alert.present();
   });
  }

  uploadImage(){
    //Show loading
    let loader = this.loadingCtrl.create({
      content: "A carregar..."
    });
    loader.present();

    var userdata = localStorage.getItem('userData');
    let user = JSON.parse(userdata);
    // console.log(user.id);
    this.qual = false;
    this.user.foto_perfil = this.myphoto;

    let postData = {
      "foto": this.myphoto,
      "idUser": user.id
    } 


    this.provedor.setfotoperfil(postData).then((result) => {
      this.responseData = result;
      if(this.responseData.result.created=="1"){
        const alert = this.alertCtrl.create({
          title: 'Sucesso!',
          subTitle: 'A publicação foi realizada com sucesso.',
          buttons: ['OK']
        });
        loader.dismiss();
        alert.present();
        //this.navCtrl.push(TabsuserPage);
      }
      else{ 

        const alert = this.alertCtrl.create({
          title: 'Falha!',
          subTitle: 'Desculpa, ocorreu um erro ao realizar seu pedido.',
          buttons: ['OK']
        });
        loader.dismiss();
        alert.present();
      }
    }, (err) => {
      const alert = this.alertCtrl.create({
        title: 'Erro!',
        subTitle: 'Desculpa, ocorreu um erro ao realizar seu pedido.',
        buttons: ['OK']
      });
      loader.dismiss();
      alert.present();
    });
  }

  setPassword(idU, idP){
    this.provedor.setPassword(idU, idP).subscribe();
  }

  showPrompt() {
    const prompt = this.alertCtrl.create({
      title: 'Palavra Passe',
      message: "Introduza a nova palavra passe",
      inputs: [
      {
        name: 'password',
        type: 'password',
        placeholder:'Nova Palavra Passe'
      },
      ],
      buttons: [
      {
        text: 'Cancelar',
        handler: data => {
          console.log('Cancel clicked');
        }
      },
      {
        text: 'Salvar',
        handler: data => {
          this.setPassword(this.user.id, data.password);
          const alert = this.alertCtrl.create({
            title: 'Sucesso!',
            subTitle: 'A palavra passe foi alterada com sucesso.',
            buttons: ['OK']
          });
          alert.present();
        }
      }
      ]
    });
    prompt.present();
  }

  doRefresh(refresher) {
    this.provedor.getUtilizador('getUser.php?e='+this.user.id).subscribe((result) => {
      this.user=result.records;
      localStorage.setItem('userData', JSON.stringify(this.user));
      refresher.complete();
    });
    
  }

  logout(){

    this.splashScreen.show();
    localStorage.clear();
    window.location.reload();
    //this.navCtrl.push(UtilizadorPage);
  }
}
