import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { ProvedorProvider } from '../../providers/provedor/provedor';
import { ImageViewerController } from 'ionic-img-viewer';

/**
 * Generated class for the GaleriaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@Component({
  selector: 'page-galeria',
  templateUrl: 'galeria.html',
})
export class GaleriaPage {
	galeria = [];
	tipo = "1";
	tip = "2";
  _imageViewerCtrl: ImageViewerController;

  constructor(public navCtrl: NavController, public navParams: NavParams, private provedor: ProvedorProvider, imageViewerCtrl: ImageViewerController) {
  	this.getGaleria();
    this._imageViewerCtrl = imageViewerCtrl;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GaleriaPage');
  }

  getGaleria(){
  	this.provedor.getGaleria().subscribe(data => this.galeria = data);
  }

  presentImage(myImage) {
    const imageViewer = this._imageViewerCtrl.create(myImage);
    imageViewer.present();
  }

}
