import { Component } from '@angular/core';
import { IonicPage, NavParams, ViewController } from 'ionic-angular';

/**
 * Generated class for the ModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-modal',
  templateUrl: 'modal.html',
})
export class ModalPage {
	dados: any;

  constructor(public navParams: NavParams, private view: ViewController) {
  }

  ionViewWillLoad() {
     this.dados = this.navParams.get('data'); 
  }

  closeModal(){
  	this.view.dismiss();
  }

  

}
