import { Component } from '@angular/core';
import { NavController, NavParams, ModalController } from 'ionic-angular';
import { ProvedorProvider } from '../../providers/provedor/provedor';
import { ImageViewerController } from 'ionic-img-viewer';

/**
 * Generated class for the TodosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


 @Component({
 	selector: 'page-todos',
 	templateUrl: 'todos.html',
 })
 export class TodosPage {

 	publicacoes = [];
 	public tap: number = 0;
 	idU: any;
 	lista: number = 1;
 	_imageViewerCtrl: ImageViewerController;

 	constructor(public navCtrl: NavController, public navParams: NavParams, private provedor: ProvedorProvider, private modal: ModalController, imageViewerCtrl: ImageViewerController) {
 		var userdata = localStorage.getItem('userData');
 		let user = JSON.parse(userdata);
 		this.idU = user.id;
 		this.getPublicacoes(user.id, this.lista);
 		this._imageViewerCtrl = imageViewerCtrl;
 	}

 	getPublicacoes(id, lista){
 		this.provedor.getPublicacoes(id, lista).subscribe(data => this.publicacoes = data);
 	}

 	setlike(idU, idP){
 		this.provedor.setlike(idU, idP).subscribe();
 	}

 	tapEvent(event, id, like) {

 		if(like == 'true'){
 			for(let p of this.publicacoes) {
 				if(p.id == id) {
 					p.like = 'false';
 					p.gosto = p.gosto - 1;
 					console.log(p.like);
 					this.setlike(this.idU, id);
 				}
 			}
 		}else{
 			for(let p of this.publicacoes) {
 				if(p.id == id) {
 					p.like = 'true';
 					p.gosto = p.gosto + 1;
 					console.log(p.like);
 					this.setlike(this.idU, id);
 				}
 			}
 		}	
 	}

 	openModal(t){
 		const myModal = this.modal.create('ModaltodosPage', {data: t});
 		myModal.present();
 	}

 	presentImage(myImage) {
 		const imageViewer = this._imageViewerCtrl.create(myImage);
 		imageViewer.present();
 	}

 	doRefresh(refresher) {
 		this.lista = 1;
 		this.provedor.getPublicacoes(this.idU, this.lista).subscribe(data => { this.publicacoes = data; refresher.complete();});
 	}

 	doInfinite(infiniteScroll) {
 		this.lista = this.lista + 10;
 		this.provedor.getPublicacoes(this.idU, this.lista).subscribe(data => { 
 			if(data.result == "0"){ infiniteScroll.complete();}
 			else{this.publicacoes = this.publicacoes.concat(data); infiniteScroll.complete();}
 			
 		});
 	}
 }
