import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { HomePage } from '../home/home';
import { MapaPage } from '../mapa/mapa';
import { UtilizadorPage } from '../utilizador/utilizador';

@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs.html',
})
export class TabsPage {
	tab1Root = HomePage;
	tab2Root = MapaPage;
	tab3Root = UtilizadorPage;
	myIndex: number;


  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

}
