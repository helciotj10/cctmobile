import { Component, ViewChild, ElementRef } from '@angular/core';
import { NavController, NavParams, PopoverController } from 'ionic-angular';
import { PopoverComponent } from '../../components/popover/popover';
import { ProvedorProvider } from '../../providers/provedor/provedor';

/**
 * Generated class for the HistoriaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@Component({
  selector: 'page-historia',
  templateUrl: 'historia.html',
})
export class HistoriaPage {
  foto = [];
  historia = [];
  @ViewChild('popoverContent', { read: ElementRef }) content: ElementRef;
  @ViewChild('popoverText', { read: ElementRef }) text: ElementRef;

  constructor(public navCtrl: NavController, public navParams: NavParams, public popoverCtrl: PopoverController, private provedor: ProvedorProvider) {
    this.getHistoria();
    this.getFhistoria();
  }

  getFhistoria(){
    this.provedor.getFhistoria().subscribe(data => this.foto = data);
  }

  getHistoria(){
    this.provedor.getHistoria().subscribe(data => this.historia = data);
  }

  presentPopover(myEvent) {
    let popover = this.popoverCtrl.create(PopoverComponent, {
      contentEle: this.content.nativeElement,
      textEle: this.text.nativeElement
    });
    popover.present({
      ev: myEvent
    });
  }

}
