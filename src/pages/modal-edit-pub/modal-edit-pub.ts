import { Component } from '@angular/core';
import { IonicPage, ViewController, NavParams, LoadingController, AlertController, ActionSheetController } from 'ionic-angular';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { ProvedorProvider } from '../../providers/provedor/provedor';
/**
 * Generated class for the ModalEditPubPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

 @IonicPage()
 @Component({
  selector: 'page-modal-edit-pub',
  templateUrl: 'modal-edit-pub.html',
})
 export class ModalEditPubPage {
  dados: any = [];
  myphoto:any;
  postData:any;
  responseData: any;
  data: any = [];
  qual: boolean = true;

  constructor(private view: ViewController, public navParams: NavParams, public actionSheetCtrl: ActionSheetController, private provedor: ProvedorProvider, private camera: Camera,  private loadingCtrl:LoadingController, public alertCtrl: AlertController) {
  }

  ionViewDidLoad() {
    this.dados = this.navParams.get('data'); 
    
  }

  closeModal(){
  	this.view.dismiss();
  }

  presentActionSheet() {
    const actionSheet = this.actionSheetCtrl.create({
      title: 'Carregar Imagem',
      buttons: [
      {
        text: 'Tirar Uma Foto',
        handler: () => {
          this.qual = false;
          this.takePhoto();
        }
      },{
        text: 'Adicionar da galeria',
        handler: () => {
          this.qual = false;
          this.getImage();
        }
      },{
        text: 'Cortar Imagem',
        handler: () => {
          this.qual = false;
          this.cropImage();
        }
      },{
        text: 'Cancel',
        role: 'cancel',
        handler: () => {
            //console.log('Cancel clicked');
          }
        }
        ]
      });
    actionSheet.present();
  }


  takePhoto(){
    const options: CameraOptions = {
      quality: 70,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }

    this.camera.getPicture(options).then((imageData) => {
        // imageData is either a base64 encoded string or a file URI
        // If it's base64:
        this.myphoto = 'data:image/jpeg;base64,' + imageData;
      }, (err) => {
        const alert = this.alertCtrl.create({
          title: 'Erro',
          subTitle: 'Desculpa, ocorreu um erro ao realizar seu pedido.',
          buttons: ['OK']
        });
        alert.present();
      });
  }

  getImage() {
    const options: CameraOptions = {
      quality: 70,
      destinationType: this.camera.DestinationType.DATA_URL,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      saveToPhotoAlbum:false
    }

    this.camera.getPicture(options).then((imageData) => {
                // imageData is either a base64 encoded string or a file URI
                // If it's base64:
                this.myphoto = 'data:image/jpeg;base64,' + imageData;

              }, (err) => {
               const alert = this.alertCtrl.create({
                title: 'Erro',
                subTitle: 'Desculpa, ocorreu um erro ao realizar seu pedido.',
                buttons: ['OK']
              });
               alert.present();
             });
  }

  cropImage() {
    const options: CameraOptions = {
      quality: 70,
      destinationType: this.camera.DestinationType.DATA_URL,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      saveToPhotoAlbum: false,
      allowEdit:true,
      targetWidth:300,
      targetHeight:300
    }

    this.camera.getPicture(options).then((imageData) => {
              // imageData is either a base64 encoded string or a file URI
              // If it's base64:
              this.myphoto = 'data:image/jpeg;base64,' + imageData;
            }, (err) => {
             const alert = this.alertCtrl.create({
              title: 'Erro',
              subTitle: 'Desculpa, ocorreu um erro ao realizar seu pedido.',
              buttons: ['OK']
            });
             alert.present();
           });
  }

  uploadImage(){
      //Show loading
      let loader = this.loadingCtrl.create({
        content: "A carregar..."
      });
      loader.present();

      var userdata = localStorage.getItem('userData');
      let user = JSON.parse(userdata);
     // console.log(user.id);

     if(!this.data.texto && !this.myphoto){
      const alert = this.alertCtrl.create({
        title: 'OK!',
        subTitle: 'Nenhuma alteração efetuada.',
        buttons: ['OK']
      });
      loader.dismiss();
      alert.present();
        //this.navCtrl.push(TabsuserPage);
      }else if(!this.myphoto || !this.data.texto){

        if(!this.myphoto){
          this.postData = {
              "id": this.dados.id,
              "texto": this.data.texto,
              "foto": "vaziu",
              "idUser": user.id
            }
          }else{
            this.postData = {
              "id": this.dados.id,
              "texto": "vaziu",
              "foto": this.myphoto,
              "idUser": user.id
            }
          }
             
            this.provedor.updatePublicacao(this.postData).then((result) => {
              this.responseData = result;
              if(this.responseData.result.created=="1"){
                const alert = this.alertCtrl.create({
                  title: 'Sucesso!',
                  subTitle: 'A publicação foi editada com sucesso.',
                  buttons: ['OK']
                });
                loader.dismiss();
                alert.present();
              //this.navCtrl.push(TabsuserPage);
            }
            else{ 

              const alert = this.alertCtrl.create({
                title: 'Falha!',
                subTitle: 'Desculpa, ocorreu um erro ao realizar seu pedido.',
                buttons: ['OK']
              });
              loader.dismiss();
              alert.present();
            }
          }, (err) => {
            const alert = this.alertCtrl.create({
              title: 'Erro!',
              subTitle: 'Desculpa, ocorreu um erro ao realizar seu pedido.',
              buttons: ['OK']
            });
            loader.dismiss();
            alert.present();
          });

    }else{
         this.postData = {
          "id": this.dados.id,
          "texto": this.data.texto,
          "foto": this.myphoto,
          "idUser": user.id
        }

        this.provedor.updatePublicacao(this.postData).then((result) => {
          this.responseData = result;
          if(this.responseData.result.created=="1"){
            const alert = this.alertCtrl.create({
              title: 'Sucesso!',
              subTitle: 'A publicação foi editada com sucesso.',
              buttons: ['OK']
            });
            loader.dismiss();
            alert.present();
            //this.navCtrl.push(TabsuserPage);
          }
          else{ 

            const alert = this.alertCtrl.create({
              title: 'Falha!',
              subTitle: 'Desculpa, ocorreu um erro ao realizar seu pedido.',
              buttons: ['OK']
            });
            loader.dismiss();
            alert.present();
          }
        }, (err) => {
          const alert = this.alertCtrl.create({
            title: 'Erro!',
            subTitle: 'Desculpa, ocorreu um erro ao realizar seu pedido.',
            buttons: ['OK']
          });
          loader.dismiss();
          alert.present();
        });
  }


}

}
