import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ModalEditPubPage } from './modal-edit-pub';

@NgModule({
  declarations: [
    ModalEditPubPage,
  ],
  imports: [
    IonicPageModule.forChild(ModalEditPubPage),
  ],
})
export class ModalEditPubPageModule {}
