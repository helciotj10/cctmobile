import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
declare var google;

/**
 * Generated class for the VisitaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-visita',
  templateUrl: 'visita.html',
})
export class VisitaPage {

	panorama: any;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  	const that = this;
  	setTimeout(function(){
  		that.streeView();
  	},2000);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad VisitaPage');
  }

  streeView(){
  	this.panorama = new google.maps.StreetViewPanorama(
            document.getElementById('street-view'),
            {
              position: {lat: 15.2642317, lng: -23.7450205},
              pov: {heading: 165, pitch: 0},
              zoom: 1
            });
  }

}
